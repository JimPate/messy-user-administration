# Beispielprojekt einer Benutzerverwaltung

Das Projekt ist voll von code smells, die es zu beseitigen gilt. Es steht also Refactoring an.
Zusätzlich ist die Benutzerverwaltung sehr rudimentär aufgebaut und bedarf diverser Erweiterungen.
Die Erweiterungen sollen natürlich unter Einhaltung der Clean Code Prinzipien erfolgen. 
Tests dürfen natürlich nicht vergessen werden.

## Mögliche Erweiterungen
* Einzelne Benutzer anhand der Id ausgeben
* Benutzer löschen
* Benutzer um Rollen erweitern
* Speichern der Benutzer persistieren
* Hilfefunktion als zusätzliche Option

## Mögliches Refactoring
* Anwendung des Builder Pattern
* Code aufteilen
    * Packaging
    * Aufteilung der Zuständigkeit
* Zuständigkeiten separieren
    * toString für einen Benutzer
    * Validierung
* Code testbar machen
    * Die public static void main Methode lässt sich nicht anständig testen und ist zu groß
* Tests erweitern
* Sonar Issues beheben (Optional)

## Voraussetzungen
* Java Version 8+
* Maven 3.+

## Ausführung
Die Anwendung ist als eine _Fat-Jar_ angelegt, d.h. alle Abhängigkeiten sind in der Jar enthalten.  
Im Projektordner:
* `mvn clean package`
* `java -jar target/messy-user-administration.jar`

## Reporting
### Coverage
Beim Ausführen der Tests (`mvn test`) wird mit Hilfe von Jacoco ein Reporting erstellt.
Unter **"/target/site/jacoco"** wird automatisch ein Coverage Reporting generiert und kann dazu genutzt werden, die Testabdeckung zu erhöhen.

### Code Smells
Zusätzlich zu der Testabdeckung, kann ein Codestyle-Report generiert werden.
dazu genügt folgender Befehle: `mvn pmd:pmd`
Der Report wird unter **"/target/site"** generiert.

### Sonarqube
Im Projekt ist das maven sonar plugin installiert. Dafür wird allerdings eine Laufende sonarqube Instanz benötigt.

`mvn sonar:sonar -Dsonar.host.url=<SONAR_URL> -Dsonar.login=<GENERATED-TOKEN>`
