import com.google.common.base.Strings;

import java.util.List;

public class UserService {

    private UserRepository userRepository;

    public UserService() {
        this.userRepository = new UserRepository();
    }

    private static int i = 1;

    public User addUser(String name, String surname, String email) throws ValidationException {
        if(Strings.isNullOrEmpty(name)) {
            throw new ValidationException("Username must not be empty");
        }
        if(Strings.isNullOrEmpty(surname)) {
            throw new ValidationException("Surname must not be empty");
        }
        if(Strings.isNullOrEmpty(email) || !email.contains("@")) {
            throw new ValidationException("Email must be valid and not empty");
        }
        User user = new User(i++, name, surname, email);
        userRepository.addUser(user);
        return user;
    }

    public List<User> getAllUsers() {
        return userRepository.getAllUsers();
    }
}
