import java.util.ArrayList;
import java.util.List;

public class UserRepository {

    public UserRepository() {
        this.repository = new ArrayList<>();
    }

    public boolean addUser(User user) {
        return repository.add(user);
    }

    public List<User> getAllUsers() {
        return repository;
    }

    private List<User> repository;
}
