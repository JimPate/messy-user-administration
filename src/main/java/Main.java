import java.sql.SQLException;
import java.util.Scanner;
import java.util.LinkedList;


public class Main {

    public static void main(String[] args) throws SQLException {
        UserService userService = new UserService();

        Scanner sc = new Scanner(System.in);
        System.out.println("Messy User Administartion");
        while(true) {
            System.out.println();
            System.out.println("What should I do? (Input number)");
            System.out.println("1) Show all users");
            System.out.println("2) Show details for user with id");
            System.out.println("3) create User");
            System.out.println("4) delete User");
            System.out.println("5) exit");
            int action = 0;
            try {
                action = Integer.parseInt(sc.nextLine());
            } catch(Exception e) {
            }
            if(action == 1) {
                if(userService.getAllUsers().size()>0) {
                    for(User user : userService.getAllUsers()) {
                        System.out.println("Id: " + user.getId() + ", name: " + user.getName() +
                                ", surname: " + user.getSurname()
                        );
                    }
                } else {
                    System.out.println("No users created");
                }
            }
            if(action == 2) {
                System.out.println("Not yet implemented");
            }
            if(action == 3) {
                while(true) {
                    System.out.println("Input user details in one line separated by \";\": <name>;<surname>;<email>");
                    String userDetails = sc.nextLine();
                    String[] split = userDetails.split(";");
                    if(split.length == 3) {
                        try {
                            User user = userService.addUser(split[0], split[1], split[2]);
                            System.out.println("User created: ");
                            System.out.println("Id: " + user.getId() + ", name: " + user.getName() +
                                    ", surname: " + user.getSurname() +
                                    ", email: " + user.getEmail()
                            );
                        } catch(ValidationException e) {
                            System.out.println(e.getMessage());
                        }
                        break;
                    } else {
                        System.out.println("Wrong input. Needed: <name>; <surname>; <email>");
                    }
                }
            }
            if(action == 4) {
                System.out.println("Not yet implemented");
            }
            if(action == 5) {
                System.out.printf("Good bye");
                System.exit(0);
            }
        }
    }
}
