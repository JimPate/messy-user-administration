import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    @Test
    public void shouldAddUser() throws Exception {
        userService.addUser("Foo", "Bar", "foo@bar.de");

        verify(userRepository).addUser(new User(1, "Foo", "Bar", "foo@bar.de"));
    }
}